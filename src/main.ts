import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { AppModule } from './app.module';
import compression from 'fastify-compress';
import { ConfigService } from '@nestjs/config';
import * as helmet from 'fastify-helmet';
import { join } from 'path';
import { ValidationPipe } from '@nestjs/common';
const tinify = require('tinify');

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );
  const config = app.get(ConfigService);
  await app.register(require('fastify-multipart'), {
    limits: {
      fileSize: 524288000,
    },
  });
  await app.register(compression);
  app.useStaticAssets({
    root: join(__dirname, '..', 'public'),
    prefix: '/public/',
  });
  await app.register((instance, opts, next) => {
    instance.register(require('fastify-static'), {
      root: join(__dirname, '..', 'client', 'build'),
    });
    next();
  });
  app.useGlobalPipes(new ValidationPipe());
  // @ts-ignore
  await app.register(helmet);
  await app.enableCors();
  tinify.key = config.get('tinifyKey');
  await app.listen(config.get('PORT'), '0.0.0.0');
}
bootstrap();
