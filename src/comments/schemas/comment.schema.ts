import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { User } from '../../users/schemas/user.schema';
import * as mongoose from 'mongoose';

export type CommentDocument = Comment & mongoose.Document;

@Schema()
export class Comment {
  @Prop({ default: new Date() })
  uploadedAt: Date;
  @Prop({ required: true })
  body: string;
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true })
  uploadedBy: User;
  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: 'User' })
  likes: string[];
  @Prop({ type: [mongoose.Schema.Types.ObjectId], ref: 'User' })
  dislikes: string[];
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
    required: true,
  })
  productId: string;
  @Prop()
  author: string;
  @Prop({ default: false })
  edited: boolean;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
