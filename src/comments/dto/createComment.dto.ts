import { IsNotEmpty } from 'class-validator';

export class createCommentDto {
  @IsNotEmpty()
  userId: string;
  @IsNotEmpty()
  body: string;
  @IsNotEmpty()
  productId: string;
  @IsNotEmpty()
  author: string;
}
