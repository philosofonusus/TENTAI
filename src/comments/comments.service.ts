import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CommentDocument, Comment } from './schemas/comment.schema';
import { createCommentDto } from './dto/createComment.dto';

@Injectable()
export class CommentsService {
  constructor(
    @InjectModel(Comment.name) private commentModel: Model<CommentDocument>,
  ) {}
  async createComment(createCommentDto: createCommentDto) {
    await new this.commentModel({
      uploadedBy: createCommentDto.userId,
      body: createCommentDto.body,
      author: createCommentDto.author,
      productId: createCommentDto.productId,
    }).save();
  }
  async checkCommentOwner(comment: Comment, userId: string) {
    // @ts-ignore
    if (comment.uploadedBy != userId)
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
  }
  async dislikeComment(userId: string, id: string) {
    const comment = await this.getOneComment(id);
    if (comment.dislikes.includes(userId)) {
      comment.dislikes.filter((el) => el !== userId);
    } else {
      comment.dislikes.push(userId);
    }
    comment.save();
  }
  async likeComment(userId: string, id: string) {
    const comment = await this.getOneComment(id);
    if (comment.likes.includes(userId)) {
      comment.likes.filter((el) => el !== userId);
    } else {
      comment.likes.push(userId);
    }
    comment.save();
  }
  async removeComment(id: string, userId: string) {
    const comment = await this.getOneComment(id);
    await this.checkCommentOwner(comment, userId);
    await comment.remove();
  }
  async editComment(id: string, userId: string, body: string) {
    const comment = await this.getOneComment(id);
    await this.checkCommentOwner(comment, userId);
    if (comment.body != body) comment.edited = true;
    comment.body = body;
    comment.save();
  }
  getComments(productId: string, page: number, pageSize: number) {
    return this.commentModel
      .find({ productId })
      .skip((page > 0 ? page - 1 : 0) * pageSize)
      .limit(pageSize)
      .sort({ uploadedAt: -1 });
  }
  async getOneComment(id: string) {
    const comment = await this.commentModel.findById(id);
    if (!comment)
      throw new HttpException('Comment was not found', HttpStatus.NOT_FOUND);
    return comment;
  }
}
