import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './schemas/user.schema';
import { UsersController } from './users.controller';
import { Product, ProductSchema } from '../products/schemas/product.schema';
import { CommentsModule } from '../comments/comments.module';
import { ProductsModule } from '../products/products.module';

@Module({
  providers: [UsersService],
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Product.name, schema: ProductSchema },
    ]),
    CommentsModule,
    ProductsModule,
  ],
  exports: [UsersService],
  controllers: [UsersController],
})
export class UsersModule {}
