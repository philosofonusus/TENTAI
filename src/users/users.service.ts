import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User, UserDocument } from './schemas/user.schema';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from '../globalDto/createUser.dto';
import * as bcrypt from 'bcrypt';
import { Product, ProductDocument } from '../products/schemas/product.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Product.name) private productModel: Model<ProductDocument>,
  ) {}
  findOne(query) {
    return this.userModel.findOne(query);
  }
  async addFavourite(userId: string, favouriteId: string) {
    const user = await this.userModel.findById(userId);
    if (user?.favourites.includes(favouriteId))
      throw new HttpException('Already in favourites', HttpStatus.FOUND);
    user.favourites.push(favouriteId);
    await user.save();
  }

  async removeFavourite(userId: string, favouriteId: string) {
    await this.userModel.findByIdAndUpdate(
      { _id: userId },
      { $pull: { favourites: { $in: [favouriteId] } } },
    );
  }
  async create(user: CreateUserDto) {
    return await new this.userModel({
      name: user.name,
      email: user.email,
      password: await bcrypt.hash(user.password, 10),
      role: 0,
      favourites: [],
    }).save();
  }
}
