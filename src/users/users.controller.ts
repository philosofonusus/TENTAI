import {
  Controller,
  HttpCode,
  HttpStatus,
  Get,
  UseGuards,
  Param,
  Req,
  Post,
  Body,
} from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UsersService } from './users.service';
import { CommentsService } from '../comments/comments.service';
import { ProductsService } from '../products/products.service';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly productsService: ProductsService,
    private readonly commentsService: CommentsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get('/addFavourite/:favourite')
  @HttpCode(HttpStatus.CREATED)
  async addFavourite(@Param('favourite') favourite: string, @Req() req) {
    await this.productsService.checkProductExistence(favourite);
    await this.usersService.addFavourite(req.user.userId, favourite);
    return {};
  }
  @UseGuards(JwtAuthGuard)
  @Get('/removeFavourite/:favourite')
  @HttpCode(HttpStatus.OK)
  async removeFavourite(@Param('favourite') favourite: string, @Req() req) {
    await this.usersService.removeFavourite(req.user.userId, favourite);
    return {};
  }
  @UseGuards(JwtAuthGuard)
  @Post('/comments/remove')
  @HttpCode(HttpStatus.OK)
  async removeComment(
    @Body() removeProductDto: { commentId: string },
    @Req() req,
  ) {
    await this.commentsService.removeComment(
      removeProductDto.commentId,
      req.user.userId,
    );
    return {};
  }
  @UseGuards(JwtAuthGuard)
  @Post('/comments/create')
  @HttpCode(HttpStatus.CREATED)
  async createComment(
    @Body() createCommentDto: { body: string; productId: string },
    @Req() req,
  ) {
    await this.productsService.checkProductExistence(
      createCommentDto.productId,
    );
    await this.commentsService.createComment({
      ...createCommentDto,
      userId: req.user.userId,
      author: req.user.username,
    });
    return {};
  }
  @UseGuards(JwtAuthGuard)
  @Post('/comments/edit')
  @HttpCode(HttpStatus.OK)
  async editComment(
    @Body() editCommentDto: { body: string; commentId: string },
    @Req() req,
  ) {
    await this.commentsService.editComment(
      editCommentDto.commentId,
      req.user.userId,
      editCommentDto.body,
    );
    return {};
  }
}
