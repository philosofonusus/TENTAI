import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Role } from '../../enums/role.enum';
import { Observable } from 'rxjs';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private _minRole: number = Role.User) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return request.user.role >= this._minRole;
  }
}
