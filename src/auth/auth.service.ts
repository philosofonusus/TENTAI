import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.usersService.findOne({ email });
    if (user && (await bcrypt.compare(password, user.password))) {
      const { name, email, favourites, role, _id } = user;
      return { name, email, favourites, role, _id };
    }
    return null;
  }
  async register(user: any) {
    if (
      await this.usersService.findOne({
        $or: [{ email: user.email }, { name: user.name }],
      })
    )
      throw new HttpException('User exists', HttpStatus.NOT_ACCEPTABLE);

    const db_user = await this.usersService.create(user);

    return {
      token: await this.jwtService.sign({
        sub: db_user._id,
        username: db_user.name,
        role: db_user.role,
      }),
      user: db_user,
    };
  }

  async login(user: any) {
    return {
      token: await this.jwtService.sign({
        username: user.name,
        sub: user._id,
        role: user.role,
      }),
      user,
    };
  }
}
