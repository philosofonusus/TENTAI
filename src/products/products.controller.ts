import {
  Controller,
  Get,
  Post,
  HttpCode,
  HttpStatus,
  Param,
  Body,
  Req,
  UseGuards,
} from '@nestjs/common';
import { uploadProductDto } from './dto/upload-product.dto';
import { ProductsService } from './products.service';
import { filterProductDto } from './dto/filter-product.dto';
import { searchProductDto } from './dto/search-product.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { CommentsService } from '../comments/comments.service';
import { RoleGuard } from '../auth/guards/role.guard';
import { Role } from '../enums/role.enum';
import { join } from 'path';
import { FileService } from '../file/file.service';

const fields_paths = {
  cover: 'covers',
  episodes: 'videos',
};

const hentaiFolder = `public/hentai/`;

@Controller('products')
export class ProductsController {
  constructor(
    private readonly productsService: ProductsService,
    private readonly commentsService: CommentsService,
    private readonly fileService: FileService,
  ) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  getSome(@Body() filter: filterProductDto) {
    filter.query = {};
    return this.productsService.getItems(filter);
  }
  @UseGuards(JwtAuthGuard)
  @Post('/rate')
  @HttpCode(HttpStatus.CREATED)
  rateProduct(
    @Body() rateProductDto: { productId: string; rate: number },
    @Req() req,
  ) {
    return this.productsService.rateProduct({
      userId: req.user.userId,
      productId: rateProductDto.productId,
      rate: rateProductDto.rate,
    });
  }
  @Post('/getComments')
  @HttpCode(HttpStatus.OK)
  getProductComments(
    @Body()
    getCommentsDto: {
      page: number;
      pageSize: number;
      productId: string;
    },
  ) {
    return this.commentsService.getComments(
      getCommentsDto.productId,
      getCommentsDto.page,
      getCommentsDto.pageSize,
    );
  }
  @Get('/item/:id')
  @HttpCode(HttpStatus.OK)
  getOneById(@Param('id') id: string) {
    return this.productsService.getOneById(id);
  }
  @Post('/search')
  @HttpCode(HttpStatus.OK)
  search(@Body() filter: searchProductDto) {
    const TitleRegex = new RegExp(filter.searchQuery, 'ig');
    const dbQuery: any = {
      $or: [{ title: TitleRegex }, { alternativeNames: TitleRegex }],

    };
    if (filter.searchTags.length)
      dbQuery.tags = {
        $all: [...filter.searchTags],
      };
    return this.productsService.getItems({ ...filter, query: dbQuery });
  }
  @UseGuards(JwtAuthGuard, new RoleGuard(Role.Admin))
  @Get('/remove/:id')
  @HttpCode(HttpStatus.OK)
  remove(@Param('id') id: string) {
    return this.productsService.removeProduct(id);
  }
  @UseGuards(JwtAuthGuard, new RoleGuard(Role.Admin))
  @Post('/upload')
  @HttpCode(HttpStatus.CREATED)
  async uploadProduct(@Body() productDto: uploadProductDto) {
    return this.productsService.upload(productDto);
  }
  @UseGuards(JwtAuthGuard, new RoleGuard(Role.Admin))
  @Post('/uploadFiles')
  @HttpCode(HttpStatus.CREATED)
  async uploadFiles(@Req() req) {
    const parts = await req.files();
    for await (const part of parts) {
      const filepath = join(
        __dirname,
        '..',
        '..',
        hentaiFolder,
        fields_paths[part.fieldname],
        part.filename,
      );
      await this.fileService.uploadFile(part, filepath);
    }
    return {};
  }
}
