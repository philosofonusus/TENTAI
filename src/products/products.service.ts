// @ts-nocheck
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { uploadProductDto } from './dto/upload-product.dto';
import { searchProductDto } from './dto/search-product.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Product, ProductDocument } from './schemas/product.schema';
import { filterProductDto } from './dto/filter-product.dto';
import { Model } from 'mongoose';
import { rateProductDto } from './dto/rate-product.dto';

const path = require('path');

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private productModel: Model<ProductDocument>,
  ) {}

  countItems(query: object | undefined) {
    return this.productModel.find(query).count();
  }
  // genThumbnails() {}
  async removeProduct(id: string) {
    try {
      const product = await this.getOneById(id);
      await this.fileService.removeFile(
        path.join(hentaiFolder, 'covers', product.cover),
      );
      for await (let ep of product.episodes) {
        await this.fileService.removeFile(
          path.join(hentaiFolder, 'videos', ep),
        );
      }
      product.remove();
    } catch (e) {
      console.log(e);
      return new HttpException('Something went wrong', 500);
    }
  }
  async rateProduct(rateProductDto: rateProductDto) {
    const product = await this.getOneById(rateProductDto.productId);
    product.rates[rateProductDto.userId] = rateProductDto.rate;
    const ProductRates = Object.values(product.rates);
    product.rating =
      ProductRates.reduce((el, acc) => acc + el, 0) / ProductRates.length;
    product.markModified('rates');
    await product.save();
  }
  async getItems(filter: filterProductDto) {
    try {
      const res = {
        list: await this.productModel
          .find(filter?.query, { rates: 0 })
          .skip((filter.page > 0 ? filter.page - 1 : 0) * filter.pageSize)
          .limit(+filter.pageSize),
      };

      if (filter.count) res.count = await this.countItems(filter.query);
      return res;
    } catch (e) {
      console.log(e);
      throw new HttpException(e, 500);
    }
  }
  async getOneById(id: string) {
    const product = await this.productModel.findById(id);
    if (!product)
      throw new HttpException('Product not found', HttpStatus.NOT_FOUND);
    return product;
  }
  checkProductExistence(id) {
    if (!this.productModel.findById(id))
      throw new HttpException('Product not found', HttpStatus.NOT_FOUND);
  }
  async upload(product: uploadProductDto) {
    try {
      const episodesFileNames = [];
      const coverFileName = `${product.title.replace(/\s/g, '-')}-${Math.floor(
        Math.random() * 1000,
      )}${'.' + product.cover.split('.')[1]}`;
      for (let i in product.episodes) {
        const episodeFileName = `${product.title.replace(
          /\s/g,
          '-',
        )}-${Math.floor(Math.random() * 1000)}${
          '.' + product.episodes[i].split('.')[1]
        }`;
        episodesFileNames.push(episodeFileName);
      }
      const hentai = new this.productModel({
        title: product.title,
        description: product.description,
        cover: coverFileName,
        episodes: episodesFileNames,
        tags: product.tags,
        releaseDate: product.releaseDate,
        alternativeNames: product.alternativeNames,
        studio: product.studio,
      });
      await hentai.save();
      return { coverFileName, episodesFileNames };
    } catch (e) {
      console.log(e);
      return new HttpException('Something went wrong', 500);
    }
  }
}
