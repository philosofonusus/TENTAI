import { IsNotEmpty } from 'class-validator';

export class uploadProductDto {
  @IsNotEmpty()
  readonly title: string;
  @IsNotEmpty()
  readonly description: string;
  @IsNotEmpty()
  readonly studio: string;
  @IsNotEmpty()
  readonly releaseDate: Date;
  @IsNotEmpty()
  readonly tags: string[];
  @IsNotEmpty()
  readonly alternativeNames: string[];
  @IsNotEmpty()
  episodes: string[];
  @IsNotEmpty()
  cover: string;
}
