import { filterProductDto } from './filter-product.dto';

export class searchProductDto extends filterProductDto {
  readonly searchQuery: string;
  readonly searchTags: string[];
  readonly pageSize?: number;
  readonly page?: number;
  readonly count: boolean = false;
}
