import { IsNotEmpty, Max, Min } from 'class-validator';

export class rateProductDto {
  @IsNotEmpty()
  userId: string;
  @IsNotEmpty()
  productId: string;
  @IsNotEmpty()
  @Min(1)
  @Max(10)
  rate: number;
}
