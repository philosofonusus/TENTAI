export class filterProductDto {
  query: object;
  count: boolean = false;
  page?: number = 1;
  pageSize?: number;
}
