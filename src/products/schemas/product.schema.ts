import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Comment } from '../../comments/schemas/comment.schema';
import { Document } from 'mongoose';

export type ProductDocument = Product & Document;

@Schema()
export class Product {
  @Prop({ required: true })
  title: string;
  @Prop({ required: true })
  studio: string;
  @Prop({ required: true })
  cover: string;
  @Prop({ required: true })
  description: string;
  @Prop({ required: true })
  tags: string[];
  @Prop({ required: true })
  episodes: string[];
  @Prop({ default: new Date() })
  uploadedAt: number;
  @Prop({ required: true })
  releaseDate: string;
  @Prop({ default: 10 })
  rating: number;
  @Prop()
  alternativeNames: string[];
  @Prop({ type: Object })
  rates;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
