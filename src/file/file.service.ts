import { HttpException, Injectable } from '@nestjs/common';

const { promisify } = require('util');
const fs = require('fs');
const pump = promisify(require('stream').pipeline);
const img_mimetypes = ['image/jpeg', 'image/png'];

const tinify = require('tinify');

@Injectable()
export class FileService {
  async imgOptimizer(file, filepath) {
      await tinify.fromFile(filepath).toFile(filepath);
  }
  async removeFile(filepath) {
    fs.unlinkSync(filepath);
  }
  async uploadFile(file, filepath) {
    try {
      await pump(file.file, fs.createWriteStream(filepath));
      if (img_mimetypes.includes(file.mimetype)) {
        await this.imgOptimizer(file, filepath);
      }
      return 'success';
    } catch (e) {
      console.log(e);
      return new HttpException('Something went wrong', 500);
    }
  }
}
