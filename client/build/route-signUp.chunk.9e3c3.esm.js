(window.webpackJsonp=window.webpackJsonp||[]).push([[8],{Fgf7:function(e,t,n){"use strict";let o;const r=n("UutA").a.button(o||(o=(e=>e)`
  background: #FFFFFF;
  border: 3px solid #FFFFFF;
  box-sizing: border-box;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 5px;
  font-family: "Poppins", sans-serif;
  font-style: normal;
  font-weight: bold;
  padding: 7px 70px;
  outline: none;
  cursor: pointer;
  text-decoration: none;
  transform: translateY(0);
  font-size: 30px;
  color: #2F2C01;
  line-height: 45px;
  transition: 0.3s ease-in-out;
  text-align: center;
  &:hover{
    transform: translateY(-10px);
  }
`));t.a=r},J93S:function(e,t,n){"use strict";let o;const r=n("UutA").a.input(o||(o=(e=>e)`
    padding: 23px 23px;
    width: 100%;
    background: transparent;
    border: 3px solid #FFFFFF;
    box-sizing: border-box;
    box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
    font-size: 23px;
    color: white;
    font-family: "Rubik", sans-serif;
    font-style: normal;
    outline: none;
    font-weight: bold;
    line-height: 27px;
    ::placeholder{
      color: white;
      font-family: "Rubik", sans-serif;
      font-weight: bold;
    }
`));t.a=r},"LP+Q":function(e,t,n){"use strict";function o(e,t){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var o=Object.getOwnPropertySymbols(e);t&&(o=o.filter((function(t){return Object.getOwnPropertyDescriptor(e,t).enumerable}))),n.push.apply(n,o)}return n}function r(e,t,n){return t in e?Object.defineProperty(e,t,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[t]=n,e}var a=n("9uw5"),i=n("XxWI");t.a=e=>async t=>{const n=await Object(i.a)("/auth/register","POST",function(e){for(var t=1;t<arguments.length;t++){var n=null!=arguments[t]?arguments[t]:{};t%2?o(Object(n),!0).forEach((function(t){r(e,t,n[t])})):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):o(Object(n)).forEach((function(t){Object.defineProperty(e,t,Object.getOwnPropertyDescriptor(n,t))}))}return e}({},e));n&&t({type:a.i,payload:{data:n,remember:"0"}})}},R6F2:function(e,t,n){"use strict";n.r(t),function(e){function o(e,t){var n=Object.keys(e);if(Object.getOwnPropertySymbols){var o=Object.getOwnPropertySymbols(e);t&&(o=o.filter((function(t){return Object.getOwnPropertyDescriptor(e,t).enumerable}))),n.push.apply(n,o)}return n}function r(e){for(var t=1;t<arguments.length;t++){var n=null!=arguments[t]?arguments[t]:{};t%2?o(Object(n),!0).forEach((function(t){a(e,t,n[t])})):Object.getOwnPropertyDescriptors?Object.defineProperties(e,Object.getOwnPropertyDescriptors(n)):o(Object(n)).forEach((function(t){Object.defineProperty(e,t,Object.getOwnPropertyDescriptor(n,t))}))}return e}function a(e,t,n){return t in e?Object.defineProperty(e,t,{value:n,enumerable:!0,configurable:!0,writable:!0}):e[t]=n,e}var i=n("l8WD"),c=n("J93S"),s=n("oGrG"),l=n.n(s),p=n("XoKQ"),u=n("oNR1"),b=n("USb2"),_=n("Fgf7"),f=n("YyTe"),h=n("/m4v"),g=n("LP+Q");t.default=Object(h.b)(e=>({user:!!e.user}))(({user:t})=>{if(t)return e(u.a,{to:"/"});const n=Object(h.c)(),o=Object(u.g)(),[a,s]=Object(i.useState)(!1),[d,O]=Object(i.useState)({email:"",password:""}),y=e=>{O(r(r({},d),{},{[e.target.name]:e.target.value}))};return e("div",{class:l.a.container},e("div",{class:l.a.signContainer},e(b.b,{to:"/",class:p.logo__word},"TENTAI"),e(c.a,{onChange:y,placeholder:"Email",type:"email",name:"email"}),e(c.a,{onChange:y,placeholder:"Username",type:"text",name:"name"}),e(c.a,{onChange:y,placeholder:"Password",type:"password",name:"password"}),e(f.a,{active:a,setActive:s,text:e("span",null,"I agree to the ",e(b.b,{class:l.a.signContainer__link_a,to:"/"},"Privacy Policy"))}),e(_.a,{onClick:()=>(a&&n(Object(g.a)(d)),o.length>2?o.goBack():o.push("/"))},"Sign Up"),e("span",{class:l.a.signContainer__link},"Already have an account?",e(b.b,{to:"/login",class:l.a.signContainer__link_a}," Sign in!"))))})}.call(this,n("hosL").h)},XoKQ:function(e){e.exports={logo:"logo__1G9oD",logo__word:"logo__word__3do2X"}},YyTe:function(e,t,n){"use strict";(function(e){n("l8WD");var o=n("ZZmT"),r=n.n(o);t.a=({active:t,text:n,setActive:o})=>e("div",{class:r.a.checkContainer},e("input",{type:"checkbox",checked:t,onClick:()=>o(!t),class:`${r.a.checkContainer__check} ${t&&r.a.checkContainer__check_active}`}),e("div",{class:r.a.checkContainer__text},n))}).call(this,n("hosL").h)},ZZmT:function(e){e.exports={checkContainer:"checkContainer__15S8g",checkContainer__check:"checkContainer__check__3sGpO",checkContainer__text:"checkContainer__text__30oYb",checkContainer__check_active:"checkContainer__check_active__1tQrF"}},oGrG:function(e){e.exports={container:"container__1VKlT",signContainer:"signContainer__3T3OI",signContainer__link:"signContainer__link__3nAH0",signContainer__link_a:"signContainer__link_a__1fG5C"}}}]);
//# sourceMappingURL=route-signUp.chunk.9e3c3.esm.js.map
