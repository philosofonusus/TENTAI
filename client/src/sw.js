import { getFiles, setupPrecaching, setupRouting } from 'preact-cli/sw';
import { registerRoute } from 'workbox-routing'
import {StaleWhileRevalidate} from 'workbox-strategies';
import { ExpirationPlugin } from 'workbox-expiration'

registerRoute(
  ({url}) => url.origin === 'https://fonts.googleapis.com' ||
    url.origin === 'https://fonts.gstatic.com',
  new StaleWhileRevalidate({
    cacheName: 'google-fonts',
    plugins: [
      new ExpirationPlugin({maxEntries: 20}),
    ],
  }),
);

setupRouting();
setupPrecaching(getFiles());

