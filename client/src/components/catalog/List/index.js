import React from 'preact/compat'
import styles from './style.css'
import ListCard from "./ListCard";

const List = ({list}) => {
    return(
        <div class={styles.list}>
            {list?.map(el => {
                if(el) return <ListCard key={el.title} productData={el}/>
            })}
        </div>
    )
}


export default List
